<?php
namespace App\Message;

class SendLogs
{
    private $userId;
    private $operationType;

    public function __construct(string $userId, string $operationType)
    {
        $this->userId = $userId;
        $this->operationType = $operationType;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function getOperationType()
    {
        return $this->operationType;
    }
}
