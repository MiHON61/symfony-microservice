<?php
namespace App\EventListener;

use App\Entity\User;
use App\Message\SendLogs;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Psr\Container\ContainerInterface;
use Symfony\Component\Cache\Adapter\RedisAdapter;
use Symfony\Component\Cache\Adapter\RedisTagAwareAdapter;
use Symfony\Component\Messenger\MessageBusInterface;

class UsersChangedListener
{
    public function __construct(ContainerInterface $container)
    {
        $this->cache = new RedisTagAwareAdapter(RedisAdapter::createConnection('redis://172.24.0.5:6379'));
        $this->conn = $container->get('doctrine.dbal.clickhouse_connection');
    }

    /**
     * Clearing users cahe and sending log to clickhouse after creating user
     * @param  App\Entity\User $user
     * @param  App\Validation\Users\CreateRequest $request
     */
    public function postPersist(User $user, LifecycleEventArgs $event, MessageBusInterface $bus): void
    {
        $this->clearUsersCache();
        $bus->dispatch(new SendLogs($user->getId(), 'create'));
    }

    /**
     * Clearing users cahe and sending log to clickhouse after updating user
     * @param  App\Entity\User $user
     * @param  App\Validation\Users\CreateRequest $request
     */
    public function postUpdate(User $user, LifecycleEventArgs $event, MessageBusInterface $bus): void
    {
        $this->clearUsersCache();
        $bus->dispatch(new SendLogs($user->getId(), 'update'));
    }

    /**
     * Clearing users cache
     */
    private function clearUsersCache():void
    {
        $this->cache->deleteItem('users');
    }
}
