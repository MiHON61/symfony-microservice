<?php
namespace App\MessageHandler;

use App\Message\SendLogs;
use Psr\Container\ContainerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class SendLogsHandler implements MessageHandlerInterface
{
    public function __construct(ContainerInterface $container)
    {
        $this->conn = $container->get('doctrine.dbal.clickhouse_connection');
    }

    public function __invoke(SendLogs $message)
    {
        $qb = $this->conn->createQueryBuilder();

        $qb
            ->insert('user_logs')
            ->setValue('user_id', ':user_id')
            ->setValue('id', ':id')
            ->setValue('type', ':type')
            ->setParameter('user_id', $message->getUserId())
            ->setParameter('id', uniqid())
            ->setParameter('type', $message->getOperationType());

        $qb->execute();
    }
}
