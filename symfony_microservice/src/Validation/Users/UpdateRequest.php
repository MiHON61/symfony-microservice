<?php
namespace App\Validation\Users;

use App\Http\RequestDTOInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
    
    class UpdateRequest implements RequestDTOInterface
    {
    
        /**

         * @Assert\Length(min="3", max="30")
         * @var string
         */
        public $username;
    
        /**

         * @Assert\Regex("/^((\+?\d{1,3})?[\(\- ]?\d{3,5}[\)\- ]?)?(\d[.\- ]?\d)+$/")
         * @var string
         */
        public $phone;

        public function __construct(Request $request)
        {
            $this->username = $request->get('username');
            $this->phone = $request->get('phone');
        }
    
        public function username(): string
        {
            return $this->username;
        }

        public function phone(): string
        {
            return $this->phone;
        }
    }
