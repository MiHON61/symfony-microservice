<?php

namespace App\Controller;

use App\Entity\User;
use App\Validation\Users\CreateRequest;
use App\Validation\Users\UpdateRequest;
use Psr\Container\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Cache\Adapter\RedisAdapter;
use Symfony\Component\Cache\Adapter\RedisTagAwareAdapter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    private $cache;

    public function __construct(ContainerInterface $container)
    {
        $this->cache = new RedisTagAwareAdapter(RedisAdapter::createConnection('redis://172.24.0.5:6379'));
    }

    /**
     * Validating data and creating user
     * @param  App\Validation\Users\CreateRequest $request
     * @return Symfony\Component\HttpFoundation\Response
     */

    /**
     * @Route("/user", methods={"POST"})
     */
    public function createUser(CreateRequest $request, MessageBusInterface $bus): Response
    {
        $entityManager = $this->getDoctrine()->getManager('default');

        $user = new User();
        $user->setUsername($request->username);
        $user->setPhone($request->phone);

        $entityManager->persist($user);
        $entityManager->flush();
        return new Response();
    }

    /**
     * Validating data and updating user
     * @param  int $id
     * @param  App\Validation\Users\UpdateRequest $request
     * @return Symfony\Component\HttpFoundation\Response
     */

    /**
     * @Route("/user/{id}", name="user", methods={"PUT"})
     */
    public function updateUser(int $id, UpdateRequest $request): Response
    {
        $entityManager = $this->getDoctrine()->getManager('default');
        $user = $entityManager->getRepository(User::class)->find($id);

        if (!$user) {
            throw $this->createNotFoundException(
                'No user found for id '.$id
            );
        }
        
        if ($request->username) {
            $user->setUsername($request->username);
        }

        if ($request->phone) {
            $user->setPhone($request->phone);
        }

        $entityManager->flush();

        return new Response();
    }

    /**
     * Returning users
     * @return Symfony\Component\HttpFoundation\Response
     */

    /**
     * @Route("/users", methods={"GET","HEAD"})
     */
    public function getUsers(): Response
    {
        $repository = $this->getDoctrine()->getRepository(User::class, 'default');
        $cachedItem = $this->cache->getItem('users');

        if (!$cachedItem->isHit()) {
            $users = $repository->findAll();
            $cachedItem->set($users);
            $cachedItem->expiresAfter(60);
            $this->cache->save($cachedItem);
        }

        $cachedItem = $this->cache->getItem("users");
        $serializer = $this->container->get('serializer');
        $reports = $serializer->serialize($cachedItem, 'json');
        return new Response($reports);
    }
}
